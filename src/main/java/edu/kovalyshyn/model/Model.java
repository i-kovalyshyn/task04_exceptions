package edu.kovalyshyn.model;

import edu.kovalyshyn.my_exception.GreaterThanHundredException;
import edu.kovalyshyn.my_exception.IntOddNumberException;
import edu.kovalyshyn.my_exception.NotPalindromeException;

public interface Model {

  void isPalindrome(String text) throws NotPalindromeException;

  void randomNumber(int number) throws IntOddNumberException, GreaterThanHundredException;

  void outOfMemoryErrorExample();


}
