package edu.kovalyshyn.model;

import edu.kovalyshyn.my_exception.GreaterThanHundredException;
import edu.kovalyshyn.my_exception.IntOddNumberException;
import edu.kovalyshyn.my_exception.NotPalindromeException;

public class BusinessLogic implements Model {

  @Override
  public void isPalindrome(String text) throws NotPalindromeException {
    text = text.replaceAll("\\W", "");//delete unnecessary symbols
    StringBuilder strBuilder = new StringBuilder(text);
    strBuilder.reverse();
    String invertedText = strBuilder.toString();

    if (text.equalsIgnoreCase(invertedText)) {
      System.out.println("Yes, it is palindrome!");
    } else {
      throw new NotPalindromeException("Exception: your text is not palindrome :(");
    }

  }

  /***
   * This randomNumber method shows the use of my own Exceptions.
   * There were checked random number from the input the user.
   * Is expected even number and less than 100.
   *
   * @param number
   * @throws IntOddNumberException
   * @throws GreaterThanHundredException
   */
  @Override
  public void randomNumber(int number) throws IntOddNumberException, GreaterThanHundredException {
    if (number % 2 == 0) {
      System.out.println("Cool! You imputed even number. \u263A");
    } else if (number > 100) {
      throw new GreaterThanHundredException("It's my Exception: You input number greater than 100");
    } else {
      throw new IntOddNumberException("It's my Exception: You imputed odd number \uD83D\uDE40");
    }

  }

  @Override
  public void outOfMemoryErrorExample() {
    Integer[] integers = new Integer[100000 * 100000];
    System.out.println(integers);

  }
}
