package edu.kovalyshyn.service;

import java.util.Optional;

public class CheckOptionalNullImpl implements CheckOptionalNull{


  @Override
  public void checkOnNull(Object object) {
    Optional.ofNullable(object).filter(i -> (!i.equals("null")))
        .orElseThrow(() -> new NullPointerException("You entered null!"));

  }

  @Override
  public void close() throws Exception {
    System.out.println("from close -> End");

  }
}
