package edu.kovalyshyn.service;

public interface CheckOptionalNull extends AutoCloseable {
  void checkOnNull(Object object);

  void close() throws Exception;

}
