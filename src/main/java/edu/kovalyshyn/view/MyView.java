package edu.kovalyshyn.view;

import edu.kovalyshyn.controller.Controller;
import edu.kovalyshyn.controller.ControllerImpl;
import edu.kovalyshyn.my_exception.GreaterThanHundredException;
import edu.kovalyshyn.my_exception.IntOddNumberException;
import edu.kovalyshyn.my_exception.NotPalindromeException;
import java.util.InputMismatchException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

  private Controller controller;
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Scanner input = new Scanner(System.in);

  public MyView() {
    controller = new ControllerImpl();
    menu = new LinkedHashMap<>();
    menu.put("1", " 1 - check for the palindrome");
    menu.put("2", " 2 - check random number");
    menu.put("3", " 3 - !!!to crash my program!!!");
    menu.put("4", " 4 - check for AutoCloseable interface");
    menu.put("Q", " Q - EXIT");

    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::pressButton1);
    methodsMenu.put("2", this::pressButton2);
    methodsMenu.put("3", this::pressButton3);
    methodsMenu.put("4", this::pressButton4);

  }

  private void pressButton1() {
    try (Scanner scanner = new Scanner(System.in)) {
      System.out.print("Enter the text to check for the palindrome: ");
      String text = scanner.nextLine();
      controller.isPalindrome(text);
    } catch (NotPalindromeException e) {
      System.out.println(e.getMessage());
    }

  }

  private void pressButton2() {
    try (Scanner scanner = new Scanner(System.in)) {
      System.out.print("Enter any number: ");
      int number = scanner.nextInt();
      controller.randomNumber(number);
    } catch (IntOddNumberException | GreaterThanHundredException e) {
      System.out.println(e.getMessage());
    }
  }

  private void pressButton3() {
    controller.outOfMemoryErrorExample();

  }

  private void pressButton4(){
   controller.checkOnNull(input);
  }

  private void outputMenu() {
    System.out.println("\n MENU:");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.println("Please, select menu point.");
      keyMenu = input.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (InputMismatchException e) {
        e.printStackTrace();
      }
    } while (!keyMenu.equals("Q"));
  }

}
