package edu.kovalyshyn.controller;

import edu.kovalyshyn.my_exception.GreaterThanHundredException;
import edu.kovalyshyn.my_exception.IntOddNumberException;
import edu.kovalyshyn.my_exception.NotPalindromeException;

public interface Controller {

  void isPalindrome(String text) throws NotPalindromeException;

  void randomNumber(int number) throws IntOddNumberException, GreaterThanHundredException;

  void outOfMemoryErrorExample();

  void checkOnNull(Object object);

}
