package edu.kovalyshyn.controller;

import edu.kovalyshyn.model.BusinessLogic;
import edu.kovalyshyn.model.Model;
import edu.kovalyshyn.my_exception.GreaterThanHundredException;
import edu.kovalyshyn.my_exception.IntOddNumberException;
import edu.kovalyshyn.my_exception.NotPalindromeException;
import edu.kovalyshyn.service.CheckOptionalNull;
import edu.kovalyshyn.service.CheckOptionalNullImpl;
import java.util.Scanner;

public class ControllerImpl implements Controller {

  private Model model;

  public ControllerImpl() {
    model = new BusinessLogic();
  }

  @Override
  public void isPalindrome(String text) throws NotPalindromeException {
    model.isPalindrome(text);
  }

  @Override
  public void randomNumber(int number) throws IntOddNumberException, GreaterThanHundredException {
    model.randomNumber(number);
  }

  @Override
  public void outOfMemoryErrorExample() {
    model.outOfMemoryErrorExample();
  }

  @Override
  public void checkOnNull(Object object) {
    Scanner scanner = new Scanner(System.in);
    System.out.println("Enter an object: ");
    object = scanner.next();
    try (CheckOptionalNull checkOptionalNull= new CheckOptionalNullImpl()) {
      checkOptionalNull.checkOnNull(object);
      System.out.println("You entered not null");
    } catch (Exception e) {
      e.printStackTrace();

       }

  }
}
