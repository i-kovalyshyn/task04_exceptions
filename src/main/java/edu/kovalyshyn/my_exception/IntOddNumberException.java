package edu.kovalyshyn.my_exception;

public class IntOddNumberException extends Exception {

  public IntOddNumberException(String message) {
    super(message);
  }

}
