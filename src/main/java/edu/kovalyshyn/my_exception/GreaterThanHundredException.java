package edu.kovalyshyn.my_exception;

public class GreaterThanHundredException extends Exception {

  public GreaterThanHundredException(String message) {
    super(message);
  }

}
