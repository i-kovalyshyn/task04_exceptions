package edu.kovalyshyn.my_exception;

public class NotPalindromeException extends Exception {

  public NotPalindromeException(String message) {
    super(message);
  }

}
