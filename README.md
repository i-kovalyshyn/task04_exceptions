﻿Task

 1. Add custom exceptions (extend Exception and
RuntimeException).

 2. Create your own AutoCloseable class, and try how it
works in try-with-resources.

 3. Throw some exception in close() method of your
AutoClosable class and watch what will happen.